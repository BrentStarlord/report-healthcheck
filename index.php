<?php
date_default_timezone_set('UTC');
?>

<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Marketplace Health Check</title>
  <meta name="description" content="checking the health of Marketplace">
  <meta name="author" content="Brent Starling">
  <meta http-equiv="refresh" content="60" />
  <link href="https://fonts.googleapis.com/css?family=Roboto|Open+Sans" rel="stylesheet">
  <link rel="stylesheet" href="css/main.css?v=1.0">
  <link rel="stylesheet" href="css/styles.css?v=1.0">
</head>

<body>
  <?php
  $json = json_decode(file_get_contents('https://gateway-sb.rentalcars.com/health'), TRUE);
  //print_r($json);
  ?>
  <div class='wrapper'>
    <div class="row">
      <div class="col-xs-offset-2 col-xs-8 text-center">
        <h1 class="title"><span class="title-logo"><img src="/images/mp.200w.png"></span>Service Status</h1>
        <p class="taken">Taken from <a href="https://gateway-sb.rentalcars.com/health">https://gateway-sb.rentalcars.com/health</a></p>
      </div>
    </div>
  </div>
  <div class='wrapper'>
    <div class="row">

    <?php
    foreach($json as $item) {
      $url =  parse_url($item['name'], PHP_URL_HOST);
      $url_pieces =  explode(".", $url);
      if ( $url_pieces['0'] != null || $url_pieces['0'] != '') {
      ?>
      <div class="col-xs-4">
        <div class="box box-<?php echo $item['status']; ?>">
        <p class="box--title"><?php echo str_replace('-', ' ', $url_pieces['0']); ?></p>
        <p class="box--status"><?php echo $item['status']; ?></p>
        <p class="box--info"><?php echo $item['information']; ?></p>
        <p class="box--timetaken">Time taken: <?php echo $item['timeTakenMs']; ?>ms</p>
      <?php
      // echo $item['status'];
      // echo $item['information'];
      // echo $item['timeTakenMs'];
      ?>
      </div>
    </div>
      <?php
        }
      // to know what's in $item
      // echo '<pre>';
      // var_dump($item);
      }

      //echo phpinfo();
      ?>
  </div>
  <script src="js/scripts.js"></script>
</body>
</html>

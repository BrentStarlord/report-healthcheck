var gulp = require('gulp'),
    connect = require('gulp-connect-php'),
    browserSync = require('browser-sync'),
    sass = require('gulp-sass');

gulp.task('connect-sync', function() {

  connect.server({}, function (){
    browserSync({
      proxy: '127.0.0.1:8000'
    });
  });

  gulp.watch('**/*.php').on('change', function () {
    browserSync.reload();
  });

  gulp.watch("./sass/*.scss", ['sass']);
});

gulp.task('sass', function () {
  return gulp.src('./sass/*.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('./css'))
      .pipe(browserSync.stream({match: '**/*.css'}));
});

gulp.task('build', ['sass'], function () {

});

gulp.task('default', ['build', 'connect-sync'], function () {
});
